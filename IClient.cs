﻿namespace StudioKit.CentralStudioKit.Interfaces
{
	public interface IClient
	{
		string ClientId { get; set; }
		string ClientSecret { get; set; }
	}
}