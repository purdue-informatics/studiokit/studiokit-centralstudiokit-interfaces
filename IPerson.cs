﻿using System.Collections.Generic;
using System.Security.Principal;

namespace StudioKit.CentralStudioKit.Interfaces
{
	public interface IPerson : IPrincipal
	{
		int Id { get; }
		string Username { get; }
		string FirstName { get; set; }
		string LastName { get; set; }
		IEnumerable<IEmailAddress> EmailAddresses { get; set; }
		string EmailAddressesDisplay { get; }
		string FullNameDisplay { get; }
		string Puid { get; set; }

		void TrackLogin(string userAgent);
	}
}