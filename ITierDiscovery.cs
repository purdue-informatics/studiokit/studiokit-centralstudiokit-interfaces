﻿namespace StudioKit.CentralStudioKit.Interfaces
{
	public interface ITierDiscovery
	{
		string Tier { get; }
	}
}