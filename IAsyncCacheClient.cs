﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace StudioKit.CentralStudioKit.Interfaces
{
	public interface IAsyncCacheClient : ICacheClient
	{
		Task<object> GetAsync(string key);

		Task<T> GetAsync<T>(string key);

		Task<IEnumerable<KeyValuePair<string, object>>> GetManyAsync(IEnumerable<string> keys);

		Task<bool> PutAsync(string key, object value);

		Task<bool> PutAsync(string key, object value, TimeSpan validFor);

		Task<long> IncrementAsync(string key, long delta, long initialValue);

		Task<long> DecrementAsync(string key, long delta, long initialValue);

		Task<bool> RemoveAsync(string key);

		Task PurgeByPrefixAsync(string prefix);

		Task PurgeByPrefixListAsync(IEnumerable<string> prefixEnumerable);

		Task PurgeByContainsAsync(string contains);

		Task PurgeByContainsListAsync(IEnumerable<string> contiansEnumerable);

		Task FlushAsync();
	}
}