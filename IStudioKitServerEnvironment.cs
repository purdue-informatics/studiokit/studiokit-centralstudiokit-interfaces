﻿using System.Collections.Generic;

namespace StudioKit.CentralStudioKit.Interfaces
{
	public interface IStudioKitServerEnvironment
	{
		string Name { get; }
		string MemcachedServerAddresses { get; }
		IEnumerable<string> WhitelistEmailAddresses { get; }
		string EmailServer { get; }
		string EmailServerCredentials { get; }
		bool EmailServerSsl { get; }
		string ErrorEmails { get; }
		string ItapWebConnectionString { get; }
		string Identifier { get; }
		int KalturaPartnerID { get; }
		string KalturaAdminSecret { get; }
		string KalturaUserSecret { get; }
		int KalturaAccessControlID { get; }
		int KalturaPlayerID { get; }
		int KalturaTranscodingProfileID { get; }
		string AzureCacheUrl { get; }
		string AzureCacheKey { get; }
		string GoogleClientId { get; }
		string GoogleClientSecret { get; }
		string NovoClientId { get; }
		string NovoClientSecret { get; }
		string FacebookAPI { get; }
		string FacebookSecret { get; }
		string ApplicationInsightsKey { get; }
	}
}