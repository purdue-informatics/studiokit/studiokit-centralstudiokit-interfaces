﻿using System.Collections.Generic;
using System.Net;

namespace StudioKit.CentralStudioKit.Interfaces
{
	public interface IStudioKitConfig
	{
		IStudioKitServerEnvironment EnvironmentDetail { get; }
		string CASHost { get; }
		string ApplicationName { get; set; }
		List<IPEndPoint> MemcachedServers { get; }
		bool UsingLoginPersist { get; }
	}
}