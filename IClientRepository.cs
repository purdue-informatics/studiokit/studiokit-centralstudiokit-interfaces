﻿namespace StudioKit.CentralStudioKit.Interfaces
{
	public interface IClientRepository
	{
		void Add(IClient client);

		IClient GetByClientIdentifier(string clientIdentifier);
	}
}