﻿using System;

namespace StudioKit.CentralStudioKit.Interfaces
{
	public interface ISemanticVersion
	{
		Int32 Major { get; }
		Int32 Minor { get; }
		Int32 Patch { get; }
	}
}