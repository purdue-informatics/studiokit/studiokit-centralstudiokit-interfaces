﻿namespace StudioKit.CentralStudioKit.Interfaces
{
	public interface IEmailAddress
	{
		string Address { get; set; }
		bool IsVerified { get; set; }
		string EntityDescription { get; }
		bool IsDefault { get; }
	}
}