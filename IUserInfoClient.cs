﻿using System.Collections.Generic;

namespace StudioKit.CentralStudioKit.Interfaces
{
	public interface IUserInfoClient
	{
		IPerson GetUser(string alias);

		List<IPerson> GetUserList(string[] aliasesOrIdentifiers, bool order = true);
	}
}